# README #


### What is this repository for? ###

* This is an adapted k-means algorithm to compose students into groups of fixed size. KMeansGrouping_multi_proc.py is the implementation of the algorithm, whereas the ScoreCalculatorVectorData.py is the calculation of peer heterogeneity and homogeneity.

### set up ###

* Please customize the data's directory;
* Because of the privacy issue, the data is not allowed to be presented here. Please specify the input in accordance with the data in hand.