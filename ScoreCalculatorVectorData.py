# -*- coding: utf-8 -*-
"""
Created on Wed Sep 24 23:01:52 2014

@author: zhilin
"""

import math
#from scipy.spatial import distance

def score(element1, element2, stu_features_dict):
    """ Calculate the score from profile1 to profile2 """

    gender_index=0
    longitude_index=1
    personality_index=2
    learning_goal_index=3
    language_index=4
    description_index=5
    homo_dist_list=[]
    hetero_dist_list=[]
    
    element1_2_tz_dist=cal_periodic_dist(stu_features_dict[element1][longitude_index],stu_features_dict[element2][longitude_index])
#    print 'tz:\n'+repr(element1_2_tz_dist)    
    element1_2_gender_dist=cal_one_hot_encoding_dist(stu_features_dict[element1][gender_index],stu_features_dict[element2][gender_index])
#    print 'gender:\n'+repr(element1_2_gender_dist)        
    element1_2_personality_dist=cal_one_hot_encoding_dist(stu_features_dict[element1][personality_index],stu_features_dict[element2][personality_index])
#    print 'personality:\n'+repr(element1_2_personality_dist)      
    element1_2_goal_dist=cal_one_hot_encoding_dist(stu_features_dict[element1][learning_goal_index],stu_features_dict[element2][learning_goal_index])
#    print 'goal:\n'+repr(element1_2_goal_dist)
    element1_2_description_dist=cal_one_hot_encoding_dist(stu_features_dict[element1][description_index],stu_features_dict[element2][description_index])       

    element1_2_language_dist=cal_multi_hot_encoding_dist(stu_features_dict[element1][language_index],stu_features_dict[element2][language_index])
#    print 'language:\n'+repr(element1_2_language_dist)  
    
    homo_dist_list=[element1_2_tz_dist, element1_2_goal_dist, element1_2_language_dist]
    homo_dist_list=[x**2 for x in homo_dist_list]
    homo_score=math.sqrt(sum(homo_dist_list))
    homo_score_scalarized=math.sqrt(1./(len(homo_dist_list)))*homo_score

    hetero_dist_list=[element1_2_gender_dist, element1_2_personality_dist, element1_2_description_dist]
    hetero_dist_list=[x**2 for x in hetero_dist_list]
    hetero_score=math.sqrt(sum(hetero_dist_list))
    hetero_score_scalarized=math.sqrt(1./(len(hetero_dist_list)))*hetero_score

    score=hetero_score_scalarized-homo_score_scalarized
    return score


def cal_periodic_dist(lati_longi_1, lati_longi_2):
    T=360 #a periodic feature's period
    dist=abs(lati_longi_1-lati_longi_2)
    if dist>T/2:
        dist=T-dist
    return dist/(T/2)
    
def cal_one_hot_encoding_dist(one_hot_code1, one_hot_code2):
    one_hot_code1=convert_str_to_list(one_hot_code1)
    one_hot_code2=convert_str_to_list(one_hot_code2)
    if len(one_hot_code1)==len(one_hot_code2) and len(one_hot_code1)==sum([1 for i,j in zip(one_hot_code1,one_hot_code2) if i==j]):
        return 0
    else:
        return 1
        
def cal_multi_hot_encoding_dist(one_hot_code1, one_hot_code2):
    one_hot_code1=convert_str_to_list(one_hot_code1)
    one_hot_code2=convert_str_to_list(one_hot_code2)
    for i,j in zip(one_hot_code1,one_hot_code2):
        if i==1 and j==1:
            return 0
    return 1
    
def convert_str_to_list(ele_str):
    ele_str=ele_str[1:-1]
    str_list=ele_str.split(', ')
    num_list=[int(i) for i in str_list]
    return num_list