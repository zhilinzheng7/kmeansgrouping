# -*- coding: utf-8 -*-
"""
Created on Mon Sep 22 15:44:01 2014

@author: Zhilin Zheng

this script is to make use of python's multi-processes tech to run the program
in parallel for each running time

this algorithm is originally from: https://github.com/p2pu/mechanical-mooc/blob/playwithyourmusic.org/grouping/graphcluster.py
P2PU community
"""
import sys
sys.path.append('/common_modules')
import random
import pandas as pd
import matplotlib.pyplot as plt
import pdb
import ScoreCalculatorVectorData
import time
import csv
import multiprocessing as mp
#pdb.set_trace()


class KMeansGrouping(object):
    """ This algorithm group students into a number of fixed sized groups
        The last group may contain fewer elements.
    """
    def __init__(self, group_size, adjacency_list):
        self.adjlist = adjacency_list
        self.group_size = group_size

        self.num_features=len(adjacency_list.values()[0])
        elements = adjacency_list.keys()
        # shuffle elements
        elements = random.sample(elements, len(elements))
        # initialize all individuals' score to '0'
        self.groups = [ {element: 0 for element in elements[i:i+group_size]} for i in range(0, len(elements), group_size) ]
        if len(self.groups[-1].keys())<=group_size/2:
            self.groups[-2]=dict(self.groups[-2].items()+self.groups[-1].items())
            del self.groups[-1]        
        self.update_group_scores()
#        self.groups_ave_score=0


    def update_group_scores(self):
        """ update the stored score for every element in its current group """
        for gn, group in enumerate(self.groups):
            for element in group:
                group[element] = self.group_score(element, [p for p in group if p != element])
                
    def cal_ave_group_similarity(self):
        """ ave_group_similarity is the average similarity among all groups, in the range of [0,1]"""
        ave_score=0
        for gn, group in enumerate(self.groups):
            ind_ave_scores=0
#            pdb.set_trace()
            for element in group:
#                #only for homogeneous or heterogeneous groups(without scalarized score which is supposed to be in [0,1])
#                ind_ave_scores+=math.sqrt(1./self.num_features)*group[element]/(len(group)-1)
                # this is for heterogeneous and homogeneous featureed groups
                ind_ave_scores+=group[element]/(len(group)-1)
            each_group_score=ind_ave_scores/len(group)
            ave_score+=each_group_score
        ave_score=ave_score/len(self.groups)
        return ave_score
                
                
    def group_score(self, element, group_elements):
        """ calculate the total score for a element in a group """
        f = lambda x: ScoreCalculatorVectorData.score(x, element, self.adjlist)
        return sum(map(f, group_elements))

    def get_groups(self):
        group_formation=[group_score.keys() for group_score in self.groups]
        return group_formation

    def print_groups(self):
        group_formation=[group_score.keys() for group_score in self.groups]
        print 'the group formation is: \n' + repr(group_formation)

    def step(self):
        """ step the algorithm once """
        for gn, group in enumerate(self.groups):
            for element in group:
                # calculate a group score for element in all other groups
                group_scores = [self.group_score(element, g) for g in self.groups]
                # check if another group has a higher score
                best_score = max(group_scores)
                bgi = group_scores.index(best_score)
                if bgi != gn:                        
                    # find weakest element in the other group where the current element is supposed to be
                    worst_element, worst_score = reduce(lambda x,y: x if x[1] < y[1] else y, self.groups[bgi].items())
                    # calculate score for worse_element in current group
                    old_score_1 = group[element]
                    old_score_2 = self.groups[bgi][worst_element]
                    # do swap
                    # update the group score for the two exchanged elements 
                    del group[element]
                    group[worst_element] = self.group_score(worst_element, [g for g in group if g != element])
                    del self.groups[bgi][worst_element]
                    self.groups[bgi][element] = self.group_score(element, [k for k in self.groups[bgi]])
                    # the exchange does nothing better, restore them.
                    if self.groups[bgi][element]+group[worst_element]<=old_score_1+old_score_2:
                        del group[worst_element]
                        group[element] =old_score_1
                        del self.groups[bgi][element]
                        self.groups[bgi][worst_element]=old_score_2
                    self.update_group_scores()
        # update group scores
#        self.update_group_scores()
                    
def plot_similarity_per_iteration(ave_similarity_list):
    plt.plot(range(len(ave_similarity_list)),ave_similarity_list,'k',label='k-means variant')
    plt.legend( loc='upper left' )
    plt.ylabel('global best')
    plt.xlabel('iterations')
    plt.show()                    
                    
def run_kmeans(nIteration, group_size, stu_data, output_queue):
    start_time=time.time()
    g_cluster=KMeansGrouping(group_size,thatDict)
#==============================================================================
# #       test initial ave score's value
#         initial_ave_score=g_cluster.cal_ave_group_similarity()
#         print 'initial ave score: \n' + repr(initial_ave_score)
# #       end test        
#==============================================================================
    ave_similarity_list=[]
    #timeCost_list=[]
    for iteration in xrange(0,nIteration):
        print 'iteration: '+repr(iteration)
        g_cluster.step()
        ave_similarity=g_cluster.cal_ave_group_similarity()
        ave_similarity_list.append(ave_similarity)
            
#   print ave_similarity_list
    best_fitness=ave_similarity_list[-1]
#    g_cluster.print_groups()
        
    time_cost = time.time()-start_time
    print 'K-means time cost: '+repr(time_cost)+' seconds'
        # plot similarity by interation

    output_queue.put([best_fitness, time_cost, ave_similarity_list])
        
if __name__ == "__main__":

    output=mp.Queue()
    runtimes=5
    
    print 'start k means grouping >>>'    
    working_data_dir='/home/CSV/'
    working_data= pd.read_csv(working_data_dir+'data.csv')
    thatDict={}
    for ndx, uid in enumerate(working_data['uid']):
        thatDict[uid]=[working_data['gender'][ndx],working_data['longitude'][ndx],working_data['personality'][ndx],
                      working_data['learning_goal'][ndx],working_data['language'][ndx], working_data['description_empty'][ndx]]
                     
    """k-means parameters"""
    nIteration=10
    """group composition parameters"""
    group_size=10
    
    sim_data=[['f','t(s)']]
    processes=[mp.Process(target=run_kmeans, args=(nIteration, group_size, thatDict, output)) for num_times in range(runtimes)]
    for p in processes:
        p.start()
    for p in processes:
        p.join()
    for p in processes:
        element=output.get()
        f_t=[element[0], element[1]]
        similarity_per_iteration=element[2]
        sim_data.append(f_t)
        plot_similarity_per_iteration(similarity_per_iteration)
    print 'multi-processes results: \n'+repr(sim_data)
        
        
    sim_results_dir='/home/simulation_results/csv/'
    with open(sim_results_dir+'sim_kmeans.csv', 'w') as fp:
        writer = csv.writer(fp, delimiter=',')
        writer.writerows(sim_data)
    print 'end'
